/* eslint-disable no-undef */
// const { resolve } = require('path');
const webpackMerge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const baseConfig = require('./webpack.config.base');

module.exports = webpackMerge(baseConfig, {
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    host: '0.0.0.0',
    port: 3000,
    hot: true,
  },
  plugins: [new HtmlWebpackPlugin()],
});
