/* eslint-disable no-undef */
const { resolve, join } = require('path');
const webpackMerge = require('webpack-merge');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const rootDir = resolve(__dirname, '../');

const ENTRY_FILE_NAME = 'index.js';
const SRC_DIR = 'src';
const DIST_DIR = 'dist';


module.exports = webpackMerge({
  entry: join(rootDir, SRC_DIR, ENTRY_FILE_NAME),
  output: {
    filename: ENTRY_FILE_NAME,
    path: join(rootDir, DIST_DIR, ENTRY_FILE_NAME),
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
  ],
});
